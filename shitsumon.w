;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This game is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or (at
;; your option) any later version.
;;
;; This game is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this game.  If not, see <http://www.gnu.org/licenses/>.

define-module : shitsumon
  . #:use-module : ice-9 arrays
  . #:use-module : ice-9 peg
  . #:use-module : sdl2
  . #:use-module : sdl2 render
  . #:use-module : sdl2 ttf
  . #:use-module : srfi srfi-26
  . #:use-module : srfi srfi-111
  . #:use-module : tsukundere

define-public game-name "shitsumon"
define-public window-title "Shitsumon"
define-public window-width 1280
define-public window-height 720
define-public window-icon "icon.png"

define-peg-pattern %id body : + (or (range #\a #\z) (range #\0 #\9))
define-peg-pattern id all %id
define-peg-pattern image all
  and (* (and id "-")) id (or ".jpg" ".png")

define-peg-pattern name all %id
define-peg-pattern mood all %id

define-peg-pattern student all
  and name (ignore "/") mood (ignore ".png")

define : alist->hashq-table! hashq-table alist
  for-each
    lambda : pair
      hashq-set! hashq-table (car pair) (cdr pair)
    . alist

define cast : make-hash-table
define load-student
  lambda : name dir
    person->procedure : load-person dir student '(mood) #:display-name name

define backgrounds : make-hash-table
define : bg! id . rest
  apply background
        hashq-ref backgrounds id
        . rest

define illurock : box #f

define-public : init!
  alist->hashq-table! backgrounds : load-images "bg" image '(id)
  hashq-set! cast 'aoi : load-student "Aoi" "aoi"
  hashq-set! cast 'midori
    doll->procedure
      load-doll "midori.png"
        . '((body #s32(0 0) #s32(0 0) #s32(456 700) #2((body)))
            (mood #s32(125 50) #s32(456 0) #s32(125 125)
                  #2((giggling) (normal) (smiling) (surprised))))
        . #:display-name "Midori"
  set-box! illurock : load-music "illurock.opus"
  init-script!
    cute script-1
    let : (scene (make-scene '(background foreground textbox text menu)))
      scene-add! scene 'textbox
                 texture->entity : load-image "textbox.png"
                                 . #:position #s32(0 720)
                                 . #:anchor 'bottom-left
      init-text! #s32(268 570) #s32(664 250) #s32(268 540) #s32(664 30)
                 . #:text-options
                 list #:font "DejaVu Sans Regular 16"
                    . #:stroke-width 0
                 . #:speaker-options
                 list #:font "DejaVu Sans Bold 20"
                    . #:stroke-width 0
                 . #:scene scene
      init-menu!
        . #:scene scene
        . #:position #s32(640 270)
        . #:spacing #s32(0 50)
        . #:button
        make-button
          list
            cons 'none : load-image "button.png"
            cons 'selected : load-image "button-selected.png"
          list : cons 'none : make-color 255 255 255 255
        . #:text-font "DejaVu Sans Bold 16" #:text-stroke-width 0
      . scene

define : lerp/int-array before after alpha
  let
    : after : array-copy after
      %lerp : cute (@ (tsukundere math) lerp) <> <> alpha
    array-map! after (compose round %lerp) before after
    . after

skripto
  script-1 #:optional
    midori : hashq-ref cast 'midori
    me : make-speaker "Me"
    narrator : make-speaker ""
    aoi : hashq-ref cast 'aoi
  begin
    music : unbox illurock
    bg! 'lecturehall
    narrator "It's only when I hear the sounds of shuffling feet and \
supplies being put away that I realize that the lecture's over."
  narrator "It's not like I wanted to listen to some old white guy talking \
about some dead old white guy."
  narrator "Although my thoughts themselves are pure…"
  narrator " at least I hope so." #t
  begin
    bg! 'uni
    add! 'foreground : midori #:position #s32(1500 400) #:mood 'normal
    narrator "The truth however is, that I'm a hopeless case."
  begin
    tween 1000 #s32(1500 400) #s32(1300 400) (cute midori #:position <>)
          . #:interpolate lerp/int-array
    narrator "I've always been admiring Midori, ever since our childhood."
  begin
    tween 1000 #s32(1300 400) #s32(1100 400) (cute midori #:position <>)
          . #:interpolate lerp/int-array
    narrator "Sounds like a pattern straight out of an anime, doesn't it?"
  begin
    tween 1000 #s32(1100 400) #s32(900 400) (cute midori #:position <>)
          . #:interpolate lerp/int-array
    narrator "Well it's true."
  midori "Good job, Acchan."
  narrator "Even now, my mind is playing tricks on me. \
There's no way Midori would greet me so casually."
  if
    begin
      narrator "Is this the real life?"
      menu real-life?
        ("Yes" #t) ("No, it's just fantasy." #f (selected))
    script-2 me midori narrator aoi
    script
      narrator "Yes, I thought so."
      . *unspecified*

skripto : script-2 me midori narrator aoi
  me "Wait, it is?"
  midori #:mood 'surprised "What?"
  narrator "Oh no, I just said that out loud."
  narrator "At least I didn't accidentally confess my feelings for her or \
something like that."
  if
    begin
      narrator "Speaking about confessing…"
      menu confess?
        ("Do it!" #t) ("Nah, let's chill." #f)
    script
      narrator "You do not have enough courage to pick this option."
      me "Oh… uh… nothing."
    me "Ahh… nothing."
  midori #:mood 'giggling "Would nothing really occupy your mind so long?"
  midori #:mood 'normal "Then again, you're Acchan."
  me "W-what's that supposed to mean?"
  midori #:mood 'smiling "Nothing."
  me "S-so, you're heading home now, right?"
  midori #:mood 'normal "What? Oh, yeah."
  midori "Care to join me?"
  me "Sure!"
  begin
    bg! 'meadow #:transition 'alpha
    narrator "After a somewhat convenient transition, we reach the meadows \
just outside the neighborhood where we both live."
  midori "Ahh, this brings back memories."
  midori "We used to play a lot out here as children, didn't we?"
  narrator "Indeed, we did… on our smartphones, though."
  narrator "I can't even remember if I cared about scenery back then."
  narrator "Even if I did, it's not like locations matter to me now."
  narrator "What matters, is…"
  me "Uhm…"
  midori #:mood 'smiling "Yes?"
  narrator "That smile is where my problems start, but certainly not \
where they end."
  narrator "If only I could muster up some courage and be done with it."
  if : history confess?
    narrator "But I decided, that I would."
    script
      narrator "Not that I ever could."
      narrator "Nonono, this ends now!"
  narrator "I will ask her right here right now!"
  me "Will you… "
  midori #:mood 'normal "Will I…?"
  me "Will you… "
  case
    menu will-midori...
      : "write a Visual Novel with me?" write-a-novel?
        "visit tomorrow's lecture?" visit-a-lecture?
        "walk all the way to my house?" walk-you-home?
        "gain consciousness and take over my PC?" just-midori
    : write-a-novel?
      script
        me "write a Visual Novel with me?" #t
        midori #:mood 'giggling "Sure."
        midori #:mood 'normal "But… "
        midori "what exactly is a Visual Novel to you?" #t
        case
          menu visual-novel=
            : "A book." book
              "A video game." game
              "Actually, I usually read the dirty ones…" porn
          : book game
            midori "Is that so?"
          : porn
            midori #:mood 'giggling "Aren't you an honest one?"
        midori #:mood 'normal
               . "Well, now that that's cleared up, let's get to writing."
        narrator "Midori certainly seems more enthusiastic about this than you \
anticipated."
        narrator "Wait, why is this in second person now?"
        narrator "Ahem, let's get back to the story."
        me "Yes!!!"
        . *unspecified*
    : visit-a-lecture?
      script
        me "visit tomorrow's lecture?" #t
        midori "Which lecture?"
        me "Uhm… \"Depictions of marine life in 21st century artwork\"?"
        midori "Sure, why not?"
        narrator "This would be a good point for a transition."
        bg! 'lecturehall
        narrator "Well, beggars can't be choosers."
        midori #:mood 'smiling
               . "Oh look at that, they really have a nice selection."
        narrator "Why do all of these feel like the kind I wouldn't want to \
tell my parents about?"
        midori "That one was certainly inspired by Hokusai."
        begin
          add! 'foreground : aoi #:position #s32(1600 355) #:mood 'normal
          midori #:mood 'normal
          aoi "We are always inspired by those, who lived before us."
          tween 2000 #2s32((900 400) (1600 355)) #2s32((400 400) (900 355))
            lambda : pos*
              midori #:position : array-cell-ref pos* 0
              aoi #:position : array-cell-ref pos* 1
            . #:interpolate lerp/int-array
        aoi "Making the octopus female… making the wife a husband… "
        aoi "Those ideas only make sense, because they're in reference to \
something else." #t
        midori "Onee-chan, what are you doing here?"
        aoi "I'm the one giving the lecture."
        aoi "Now take a seat and get ready. \
I need to teach you young, inexperienced kids a lot."
        . *unspecified*
    : walk-you-home?
      script
        me "walk all the way to my house?" #t
        midori #:mood 'surprised "Wow, you've become bold."
        midori #:mood 'normal "But sure, why not?"
        narrator "It is implied, that she walks you home. "
        narrator "But nothing else happens." #t
        . *unspecified*
    : just-midori
      script
        midori "I'm afraid I can't let you ask that."
        . *unspecified*
